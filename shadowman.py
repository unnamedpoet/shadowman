#!/usr/bin/python
#importing stuff we need
import subprocess
import socket
import os
import time

#variables to hold IP and port number	
ip = raw_input("Enter the ip address: ")
port = input("Enter the port: ")

#AF_INET is IPv4, AF_INET6 is IPV6, SOCK_STREAM is TCP , DGRAM is UDP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#using connect_ex() returns a error code if true (closed) or a 0 (open port) if false
#But first we want to check if the host is actaully up, or else the program just hangs

print "Let's first see if the host is up, sending ping:"
time.sleep(1)
print ""
HOST_UP = True if os.system("ping -c 1 " + ip ) is 0 else False

if HOST_UP:
    print "--- End of ping statistics-- ----"
    print ""
    print "Host is up!"
    print "Lets check if port",port,"is open"
    time.sleep(1)
    if s.connect_ex((ip,port)) ==0:
	    print " "
            print "Port", port, "is open"
            userInput = raw_input("Do you want to see what else is open: ")
            userInput = userInput.lower()
            while userInput not in {'yes', 'no'}:
                print "Please answer yes or no"
                userInput = raw_input("Do you want to see what else is open?: ")
                userInput = userInput.lower()
            if userInput == 'yes':
                def get_nmap(options, addr):
                    return subprocess.check_output(["nmap", options, addr])
                show_nmap = get_nmap('-Pn',ip)
                print show_nmap
            else:
                print "Exiting..."
                s.close()
    else:
        #print errno
        print ""
        print "Port",port, "is closed"
        s.close()
else:
    print "Host",ip,"is down"
    s.close()
